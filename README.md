<!--
**JonathanTeran/JonathanTeran** es un repositorio ✨ _especial_ ✨ porque su `README.md` (este archivo) aparece en su perfil de GitHub.

Aquí tienes algunas ideas para comenzar:

- 🔭 Actualmente estoy trabajando en ...
- 🌱 Actualmente estoy aprendiendo ...
- 👯 Estoy buscando colaborar en ...
- 🤔 Estoy buscando ayuda con ...
- 💬 Pregúntame sobre ...
- 📫 Cómo contactarme: ...
- 😄 Pronombres: ...
- ⚡ Dato curioso: ...
-->


# ¡Hola! 👋 Soy Jonathan Teran

[![LinkedIn](https://img.shields.io/badge/LinkedIn-Jonathan%20Teran-blue?style=flat-square&logo=linkedin)](https://www.linkedin.com/in/jonathan-teran3/)

---

## 👨‍💻 Sobre mí

Soy un desarrollador de software con un gran entusiasmo por la creación de soluciones tecnológicas innovadoras. Mi pasión por la programación me ha llevado a especializarme en algunas tecnologías, donde he adquirido experiencia en muchas áreas. Actualmente, me desempeño como Desarrollador y analista de sistemas.

- Desarrollo web con tecnologías como JavaScript, PHP, HTML, CSS y frameworks como Laravel y Bootstrap.
- Gestión de bases de datos relacionales como MySQL, PostgreSQL y SQL Server.
- Automatización de procesos y DevOps con Docker y GitLab CI/CD.

---

## 🚀 Lenguajes y Tecnologías



![JavaScript](https://img.shields.io/badge/-JavaScript-F7DF1E?style=flat-square&logo=javascript&logoColor=black)
![PHP](https://img.shields.io/badge/-PHP-777BB4?style=flat-square&logo=php&logoColor=white)
![Laravel](https://img.shields.io/badge/-Laravel-FF2D20?style=flat-square&logo=laravel&logoColor=white)
![SQL Server](https://img.shields.io/badge/-SQL%20Server-CC2927?style=flat-square&logo=microsoft-sql-server&logoColor=white)
![MySQL](https://img.shields.io/badge/-MySQL-4479A1?style=flat-square&logo=mysql&logoColor=white)
![PostgreSQL](https://img.shields.io/badge/-PostgreSQL-336791?style=flat-square&logo=postgresql&logoColor=white)
![Visual Basic](https://img.shields.io/badge/-Visual%20Basic-5C2D91?style=flat-square&logo=visual-studio&logoColor=white)
![GitLab](https://img.shields.io/badge/-GitLab-FCA121?style=flat-square&logo=gitlab&logoColor=white)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-7952B3?style=flat-square&logo=bootstrap&logoColor=white)
![Node.js](https://img.shields.io/badge/-Node.js-339933?style=flat-square&logo=node.js&logoColor=white)
![HTML5](https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/-CSS3-1572B6?style=flat-square&logo=css3&logoColor=white)
![Docker](https://img.shields.io/badge/-Docker-2496ED?style=flat-square&logo=docker&logoColor=white)
![Git](https://img.shields.io/badge/-Git-F05032?style=flat-square&logo=git&logoColor=white)


---


## 📫 Contacto

- 📧 [Correo electrónico](mailto:jo-teran@hotmail.com)
- 💼 [LinkedIn](https://www.linkedin.com/in/jonathan-teran3/)

---

## 📊 Estadísticas de GitLab

Puedes encontrar más información sobre mis contribuciones en GitLab [aquí](https://gitlab.com/jo.teran3).

